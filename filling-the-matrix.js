$(document).ready(function() {
    //Turning Matrix
    var directions = [
        [0, 1],
        [1, 0],
        [0, -1],
        [-1, 0]
    ];

    $('.btn').click(function() {

        $('.input-matrix-size').validate({
            debug: true
        });

        $(".input_form").hide(400);

        let input_size = $(".input-matrix-size").val();
        let input_size_num = Number(input_size);
        console.log(input_size_num);
        event.preventDefault();

        createTable(input_size_num, input_size_num);

        let matrix = matrixArray(input_size_num, input_size_num);
        let dim = input_size_num;
        let valuesNumber = dim * dim;
        let values = getValues(dim);

        fillSpirale(matrix, 0, -1, 0, input_size_num, input_size_num, values, valuesNumber);
        console.log(matrix);



    })

    /**
     * Create an matrix with 0-fields
     * @param {*} rows 
     * @param {*} columns
     * @return matrix 
     */
    function matrixArray(rows, columns) {
        var arr = new Array();
        for (var i = 0; i < columns; i++) {
            arr[i] = new Array();
            for (var j = 0; j < rows; j++) {
                arr[i][j] = 0;
            }
        }
        return arr;
    }

    /**
     * Fills an array with numbers without 6
     * @param {*} matrixDimension 
     * @return values
     */
    function getValues(matrixDimension) {
        let valuesNumber = matrixDimension * matrixDimension;

        let values = [];
        let valueToFill = 1;

        for (let i = 0; i < valuesNumber; i += 1) {
            while (valueToFill.toString().indexOf('6') !== -1) {
                valueToFill += 1;
            }
            values.push(valueToFill);
            valueToFill += 1;
        }

        return values;
    }

    /**
     * 
     * @param {*} matrix 
     * @param {*} col 
     * @param {*} row 
     * @param {*} direction 
     * @param {*} W 
     * @param {*} H 
     * @param {*} values 
     * @param {*} valuesNumber 
     */
    function fillSpirale(matrix, col, row, direction, W, H, values, valuesNumber) {

        let curdir = directions[direction];
        let count = curdir[0] ? W : H;

        if (!count){
            return;
        }
             
          
        for (let i = 0; i < count; i++) {
            
                
             col += curdir[0];
             row += curdir[1];
              //  matrix[row][col] = values[--valuesNumber];
              // I think something here I have to add setInterval 
                    
        
             var currentRow = $(".row")[row], //  current row
                 currentCell = $(currentRow).find('.column')[col]; // current cell
                    $(currentCell).text(values[--valuesNumber]);
                    $(currentCell).addClass('show');
           
        }

        direction++;
        if (direction > 3) {
            direction = 0;
        }
        W -= Math.abs(curdir[1]); // if passed vertically - then now the width is less by 1
        H -= Math.abs(curdir[0]);


        // эти коментарии - кароче оно просто по стороне делает....
       
            var interval = setTimeout(function tick() {
                fillSpirale(matrix, col, row, direction, W, H, values, valuesNumber);
                interval = setTimeout(tick, 100);
            }, 100);
        
    }

    /**
     * Same with the matrixArray()
     * @param {*} row 
     * @param {*} columns 
     */
    function createTable(row, columns) {
        for (let i = 0; i < row; i++) {
            let table = $('.my_table');
            $(table).append('<div class="row"></div>');
            for (let j = 0; j < columns; j++) {
                $('.row:last').append('<div class="column"></div>');
            }
        }
    }


});